
from ase.lattice.cubic import FaceCenteredCubic
import numpy as np





if False:
    print(rcell)
    print('rlens', rcell.lengths())

    print(wavenumbers)

    ki=1
    kj=0
    kk=0
    wavenumvec = (ki, kj, kk)
    kvec = wavenumvec @ rcell
    klen = np.sqrt( (kvec*kvec).sum() )
    print( 'rlen0', klen )


################



def get_k_vectors_by_lambda_max(cell, lambda_max, include_zero=False):
    k_cut = 1/lambda_max
    rcell = cell.reciprocal()
    wavenumbers = np.ceil(k_cut/rcell.lengths())
    kvec_list = []
    for ki in range(1-int(wavenumbers[0]), int(wavenumbers[0])):
        for kj in range(1-int(wavenumbers[1]), int(wavenumbers[1])):
            for kk in range(1-int(wavenumbers[2]), int(wavenumbers[2])):
                wavenumvec = (ki, kj, kk)
                kvec = wavenumvec @ rcell
                klen = np.sqrt( (kvec*kvec).sum() )
                if klen/k_cut <= 1.0:
                    if include_zero:
                        kvec_list.append(kvec)
                    elif not ( ki==0 and kj==0 and kk==0):
                        kvec_list.append(kvec)
            # #print(wavenumvec, kvec, klen/k_cut)
            # if kk == 0:
            #     if klen/k_cut <= 1.0:
            #         pts_in.append(wavenumvec)
            #     else:
            #         pts_out.append(wavenumvec)
    return kvec_list


def generate_longitudinal_modulations(atoms, kvec_list, strain_max = 0.1):

    pos0 = atoms.get_positions()
    im_list = []
    for kvec in kvec_list:
        kmag = np.linalg.norm(kvec)
        kunit = kvec/(kmag+1e-8)
        amplitude = strain_max/(2*np.pi*kmag)
        disp = amplitude * np.sin( 2*np.pi * np.dot(kvec,atoms.get_positions().T ) )
        displacement = np.outer(disp,kunit)
        #print(disp.shape)
        #print(displacement.shape)

        im = atoms.copy()
        im.set_positions(pos0+displacement)
        im_list.append(im)

    return im_list



if __name__=='__main__':
    from matplotlib import pyplot as plt
    size = 5
    atoms = FaceCenteredCubic(directions=[[1, 0, 0], [0, 1, 0], [0, 0, 1]],
                              symbol='Cu',
                              size=(size, size, size),
                              pbc=True)
    cell = atoms.get_cell()
    cell[1,1] = cell[1,1]*1.5
    cell[1,0] = -0.5*cell[0,0]
    atoms.set_cell(cell, scale_atoms=True)


    ###########


    lambda_max = 8.0
    ########
    strain_max=0.05


    kvec_list = get_k_vectors_by_lambda_max(atoms.get_cell(), lambda_max=lambda_max)

    im_list = generate_longitudinal_modulations(atoms, kvec_list, strain_max)



    if True:
        k_cut = 1/lambda_max
        #pts_in=   np.array(pts_in).T @ rcell
        #pts_out= np.array(pts_out) #@ rcell

        #kvecs_in = []
        #kvecs_out = []

        #for kvec in kvec_list in pts_out:
        #    kvecs_out.append(pt_out @ rcell)

        #print(atoms.cell)
        #print(rcell)
        #print(pts_in[1],kvecs_in[1])

        kvec_list = np.array(kvec_list)
        #kvecs_out = np.array(kvecs_out)

        fig, ax = plt.subplots()
        ax.scatter(kvec_list[:,0],  kvec_list[:,1],  color='b')
        #ax.scatter(kvecs_out[:,0], kvecs_out[:,1], color='r')

        ntheta = 60
        theta = np.linspace(0,2*np.pi, ntheta)
        x = k_cut*np.cos(theta)
        y = k_cut*np.sin(theta)

        ax.plot(x,y, linestyle = '--')

        from ase.visualize import view

        view(im_list)
        ax.axis('equal')
        plt.show()

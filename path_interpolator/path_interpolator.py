
import numpy as np

class PathInterpolator:

    def __init__(self,image_list, 
            kind = 'linear', 
            use_image_distance_in_spline = True, 
            with_magnetic_moments = False, 
            remove_translation=False,
            fill_value='extrapolate'):
        ''' This function interpolates a list of ASE "Atoms" to a new '''
        nimages = len(image_list)
        natoms = len(image_list[0])
    
        self.image_list = image_list
        self.with_magnetic_moments = with_magnetic_moments

        if nimages == 2:
            print ('Only 2 images, kind will be linear')
            kind = 'linear'
        elif nimages < 2:
            print('YOU NEED AT LEAST 2 IMAGES FOR INTERPOLATION!')
            assert nimages >= 2 

        from ase.geometry.geometry import find_mic
        from scipy.interpolate import interp1d

        ####################
        distance_seq = np.zeros(nimages)
        position_collection = np.zeros((natoms,3,nimages))
        image_index = 0 # for the first image, we don't need distances, just the orginal positions
        for atom_index in range(0,natoms):
            for dim_index in range(3):
                position_collection[atom_index, dim_index, image_index] = image_list[image_index].positions[atom_index, dim_index]

        for image_index in range(1,nimages):
            D = image_list[image_index].positions - image_list[image_index-1].positions
            # should we use the mean cell i-1 and cell i ?
            D_min, D_min_len =  find_mic(D, cell =  image_list[image_index].get_cell() )
            # D_min is list of minimum image vectors
            if remove_translation:
                D_min = D_min - np.mean(D_min, axis=0)
            distance =  np.sqrt((D_min**2).sum())
            distance_seq[image_index] = distance_seq[image_index-1] + distance
            for atom_index in range(0,natoms):
                for dim_index in range(3):
                    position_collection[atom_index, dim_index, image_index] = \
                        position_collection[atom_index, dim_index, image_index-1] + D_min[atom_index][dim_index]

        self.path_distances        = distance_seq
        self.scaled_path_distances = distance_seq/distance_seq.max()

        # splines have a coordinate output and input that is image number scaled from 0 to 1.
        # we could try using the RMS distance/Frobenius distance/L2 norm along the path then scale it:
        if use_image_distance_in_spline:
            seq = distance_seq/distance_seq.max()
        else:
            seq = np.linspace(0,1, nimages)

        self.spline_distances = seq

        # builds a spline for every atom's x,y,z coordinates
        self.spline_func_collection = []
        for atom_index in range(0,natoms):
            self.spline_func_collection.append([])
            for dim_index in range(3):
                func = interp1d(seq, position_collection[atom_index,dim_index], kind=kind, fill_value=fill_value)
                self.spline_func_collection[atom_index].append(func)
        ########
        if self.with_magnetic_moments :
            test_magmom = image_list[image_index].get_initial_magnetic_moments()
            if len(test_magmom.shape) == 1:
                mag_dim = 1 ###### we have collinear spin
            else:
                mag_dim = 3 ##### have NCL ...
                
            mag_collection = np.zeros((natoms,mag_dim,nimages))
            
            for image_index in range(nimages):
                mag_mom = image_list[image_index].get_initial_magnetic_moments()
                #print(mag_mom)
                if mag_dim == 1:
                    mag_collection[:,0,image_index] = mag_mom
                else:
                    mag_collection[:, :,  image_index ] = mag_mom
                
                #for atom_index in range(natoms):
                #    mag_collection[atom_index, image_index ] = mag_mom[atom_index]

            #print(mag_collection)
            self.mag_spline_func_collection = []
            for atom_index in range(natoms):
                self.mag_spline_func_collection.append([])
                for dim_index in range(mag_dim):
                    func = interp1d(seq, mag_collection[atom_index, dim_index], kind=kind, fill_value=fill_value)
                    self.mag_spline_func_collection[atom_index].append(func)
            self.magmom_dim = mag_dim
            
            #self.mag_collection = mag_collection
            
    #############################

    def __call__(self, scaled_path_distances, copy_constraints = True):
        natoms = len(self.image_list[0])
    
        new_image_list = []
        new_seq = scaled_path_distances 
        
        if self.with_magnetic_moments:
            new_mag_mom = np.zeros((natoms,self.magmom_dim))
            
        def find_nearest_im(array, value):
            array = np.asarray(array)
            im_index = (np.abs(array - value)).argmin()
            return im_index
        
        for new_image_index in range(len(scaled_path_distances )):

            new_image = self.image_list[0].copy()
            # There must be a better
            # way which will also handle lattice vector changes
            pos = new_seq[new_image_index]

            for atom_index in range(natoms):
                for dim_index in range(3):
                    new_image.positions[atom_index, dim_index] = self.spline_func_collection[atom_index][dim_index](pos)

            if self.with_magnetic_moments :
                for atom_index in range(natoms):
                    for dim_index in range(self.magmom_dim):
                        new_mag_mom[atom_index,dim_index] = self.mag_spline_func_collection[atom_index][dim_index](pos)
                
                if self.magmom_dim == 1: # converts back to 1D arrays 
                    new_image.set_initial_magnetic_moments(new_mag_mom[:,0])
                else:
                    new_image.set_initial_magnetic_moments(new_mag_mom)
                    
            if copy_constraints:
                nearest_im_index = find_nearest_im(self.spline_distances , pos )
                new_image.set_constraint(self.image_list[nearest_im_index].constraints)
            
            # now append the new image
            new_image_list.append(new_image)
                
        return new_image_list

    def subdivide_scaled_path_distances(self, subdivisions=1):
        num_im     = len(self.scaled_path_distances)
        num_im_new = (num_im)+ (num_im-1)*subdivisions
        
        # fractional positions of the subdivisions 
        # 2x subdiv => [1/3, 2/3, 3/3 ]
        # maybe move the last value out and just put it directly in the new array to avoid rounding errors?
        fracs = (np.arange(subdivisions+1)+1)/(subdivisions+1)
        
        s_new = np.zeros(num_im_new)
        s_new[0] = self.scaled_path_distances[0] # safer than assuming zero right?
        
        delta_s = np.diff(self.scaled_path_distances)
        for segment in range(num_im-1): 
            index = 1 + segment*(subdivisions+1)
            s_new[index:index+subdivisions+1] = s_new[index-1] + delta_s[segment] * fracs
        return s_new


    def subdivide(self, subdivisions=1 ):
        s = self.subdivide_scaled_path_distances(subdivisions)
        return self(s)


    def finite_difference_tangent(self, scaled_path_distance, h=1e-6):
        natoms = len(self.image_list[0])
        pos = scaled_path_distance
        
        plus  = np.zeros((natoms,3)) 
        minus = np.zeros((natoms,3))
        for atom_index in range(natoms):
            for dim_index in range(3):
                plus[ atom_index, dim_index] = self.spline_func_collection[atom_index][dim_index](pos+h)
                minus[atom_index, dim_index] = self.spline_func_collection[atom_index][dim_index](pos-h)
        T = (plus-minus)/(2*h)
        Tnormed = T/np.sqrt((T**2).sum())
        return Tnormed
    
if __name__ == '__main__':
    ##### how to use it
    from ase import io
    import numpy as np

    old_images = [  io.read('min1.CONTCAR'),
                    io.read('saddle.CONTCAR'),
                    io.read('min2.CONTCAR')]
    natoms = len(old_images[0] )
    
    collinear = False
    if collinear:
        magmoms = np.zeros(natoms)
        
        old_images[0].set_initial_magnetic_moments(magmoms)
        magmoms[-1] = 0.5
        old_images[1].set_initial_magnetic_moments(magmoms)
        magmoms[-1] = 1.0 
        old_images[2].set_initial_magnetic_moments(magmoms)
    
    else:
        magmoms = np.zeros((natoms,3))
        
        old_images[0].set_initial_magnetic_moments(magmoms)
        magmoms[-1,2] = 0.5
        old_images[1].set_initial_magnetic_moments(magmoms)
        magmoms[-1,2] = 1.0 
        old_images[2].set_initial_magnetic_moments(magmoms)
    
    my_interpolator = PathInterpolator(old_images, with_magnetic_moments = True, kind='quadratic')
    #print(my_interpolator.mag_collection)
    
    num_new_images = 5
    scaled_path_distances = np.linspace(0,1,  num_new_images )
    new_images = my_interpolator(scaled_path_distances)

    for im in new_images:
        print(im.get_initial_magnetic_moments())

    ##########
    T = my_interpolator.finite_difference_tangent(0.5)
    print(T)
    
    for index in range(num_new_images):
        scaled_path_distance=scaled_path_distances[index]
        T = my_interpolator.finite_difference_tangent(scaled_path_distance)
        new_images[index].set_velocities(T/4)
        
    from ase.visualize import view

    view(new_images)

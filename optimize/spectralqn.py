import warnings

import numpy as np
#from numpy.linalg import eigh

from ase.optimize.optimize import Optimizer
from .omega_weight_schemes import beta_grad_line_search_eigen
from .omega_weight_schemes import OWSe_ASE_BFGS

#### 'BFGS':
def get_dH_BFGS(dr,df,dg):
    a = np.dot(dr, df)
    b = np.dot(dr, dg)
    dH_BFGS = - np.outer(df, df) / a - np.outer(dg, dg) / b
    return dH_BFGS

#### 'SR1':
def get_dH_SR1(dr,df,dg):
    v = - df - dg
    #v = df - dg
    numerator = np.outer(v,v)
    denominator = np.dot(v,dr)
    q = 1e-2 #1e-2 #?


    sa = np.abs(denominator)
    sb = np.linalg.norm(dr) * np.linalg.norm(v)
    #print(sa/sb)
    if abs(sa/sb) > q:
        dH_SR1 = numerator/denominator
    else:
        dH_SR1 = None
    return dH_SR1

def compute_dHessian(r, f, rold, fold, Hold, mix = {'BFGS':0.6, 'SR1':0.4}):
    dr = r - rold
    df = f - fold

    if np.abs(dr).max() < 1e-7:
        # Same configuration again (maybe a restart):
        return

    dg = np.dot(Hold, dr) # this is the estimated change in gradient

    ##############
    dH_BFGS = get_dH_BFGS(dr,df,dg)
    dH_SR1  = get_dH_SR1(dr,df,dg)

    if dH_SR1 is not None:
        dH = mix['BFGS']*dH_BFGS + mix['SR1']*dH_SR1
    else:
        if mix['SR1']!=0.0:
            print('SR1 too unstable, switching to pure BFGS.')
        dH = dH_BFGS

    dH = (dH + dH.T)/2.0 # BFGS and SR1 are symmetric, but is that safe against rounding errors?
    return dH


class SpectralQN(Optimizer):
    # default parameters

    defaults = {**Optimizer.defaults, 'alpha': 70.0}

    def __init__(self, atoms, restart=None, logfile='-', trajectory=None,
                    maxstep=None, master=None, alpha=None,
                    mix = {'BFGS':0.60, 'SR1':0.40},
                    omega_weight_function = OWSe_ASE_BFGS,
                    damping = 1.0, # Step scale
                    H0_noise=0.000, #0.001 is a good value for troublesome systems
                    rng=np.random,
                 ):
        """BFGS optimizer.

        Parameters:

        atoms: Atoms object
            The Atoms object to relax.

        restart: string
            Pickle file used to store hessian matrix. If set, file with
            such a name will be searched and hessian matrix stored will
            be used, if the file exists.

        trajectory: string
            Pickle file used to store trajectory of atomic movement.

        logfile: file object or str
            If *logfile* is a string, a file with that name will be opened.
            Use '-' for stdout.

        maxstep: float
            Used to set the maximum distance an atom can move per
            iteration (default value is 0.2 Å).

        master: boolean
            Defaults to None, which causes only rank 0 to save files.  If
            set to true,  this rank will save files.

        alpha: float
            Initial guess for the Hessian (curvature of energy surface). A
            conservative value of 70.0 is the default, but number of needed
            steps to converge might be less if a lower value is used. However,
            a lower value also means risk of instability.
        """
        if maxstep is None:
            self.maxstep = self.defaults['maxstep']
        else:
            self.maxstep = maxstep

        if self.maxstep > 1.0:
            warnings.warn('You are using a *very* large value for '
                          'the maximum step size: %.1f Å' % maxstep)

        if alpha is None:
            self.alpha = self.defaults['alpha']
        else:
            self.alpha = alpha

        self.mix = mix
        self.omega_weight_function = omega_weight_function
        self.H0_noise= H0_noise
        self.damping = damping
        self.rng = rng

        Optimizer.__init__(self, atoms, restart, logfile, trajectory, master)

    def todict(self):
        d = Optimizer.todict(self)
        if hasattr(self, 'maxstep'):
            d.update(maxstep=self.maxstep)
        return d

    def initialize(self):
        # initial hessian
        self.H0 = np.eye(3 * len(self.atoms)) * self.alpha

        dof = 3 * len(self.atoms)
        Hn = (self.rng.random((dof,dof))-0.5)* self.alpha * self.H0_noise
        Hn=Hn+Hn.T

        self.H0+=Hn

        self.H = None
        self.r0 = None
        self.f0 = None

    def read(self):
        self.H, self.r0, self.f0, self.maxstep = self.load()

    def step(self, f=None):
        atoms = self.atoms

        if f is None:
            f = atoms.get_forces()

        # only for diagnostics
        e = atoms.get_potential_energy()

        r = atoms.get_positions()
        f = f.reshape(-1)
        self.update(r.flat, f, self.r0, self.f0)
        #omega, V = np.linalg.eigh(self.H, UPLO='U')
        omega, V = np.linalg.eigh(self.H)

        if False:
            # FUTURE: Log this properly
            # # check for negative eigenvalues of the hessian
            if any(omega < 0):
                n_negative = len(omega[omega < 0])
                msg = '** Hessian has {} negative eigenvalues. Smallest: {}'.format(
                    n_negative, omega.min())
                print(msg, flush=True)
                #print(omega[omega < 0 ])
                #print(V[omega < 0 ])
            #     if self.logfile is not None:
            #         self.logfile.write(msg)
            #         self.logfile.flush()

        # force vector in eigenbasis
        fe = V.T @ f
        ######

        beta_GLS = beta_grad_line_search_eigen(omega, fe)

        #######
        #feweight_grad_line_search = alpha_grad_line_search * np.eye(len(f))
        #feweight_direct_min_fabs_fweight =  1/np.fabs(omega)
        #feweight_direct_min_fabs_fweight = np.diag(feweight_direct_min_fabs_fweight)

        #print(omega)
        omega_weights = self.omega_weight_function(omega, fe)


        #print(np.flip(omega))
        #print(omega_scaled)
        #print(omega_weights)
        ## this works with any scheme
        omega_mat = np.diag(omega)
        weight_mat = np.diag(omega_weights)
        beta_weight_mat = (fe.T @ weight_mat @ fe)/(fe.T @ weight_mat.T @ omega_mat @ weight_mat  @ fe)
        #feweight_weight_mat = alpha_weight_mat * weight_mat

        #print(weight_mat, beta_weight_mat)
        # compute update step
        #dre =  feweight_direct_min_fabs_fweight  @  fe
        #dre = feweight_grad_line_search @ fe
        dre = beta_weight_mat * weight_mat @ fe

        print(
            '{:4d} E {: 10.4f} Fmax {: 10.4f} Eig-(min,mean,max) ({: 10.4f}, {: 10.4f}, {: 10.4f}) grad_ls_omega {: 10.4f}'.format(
               self.nsteps, e, f.max(), omega.min(), np.mean(omega), omega.max(), 1/beta_GLS)
            )


        dr = self.damping * np.dot(V,dre).reshape((-1, 3))
        ######
        steplengths = np.sqrt((dr**2).sum(1))
        dr = self.determine_step(dr, steplengths)
        atoms.set_positions(r + dr)
        self.r0 = r.flat.copy()
        self.f0 = f.copy()
        self.dump((self.H, self.r0, self.f0, self.maxstep))

    def determine_step(self, dr, steplengths):
        """Determine step to take according to maxstep

        Normalize all steps as the largest step. This way
        we still move along the eigendirection.
        """
        maxsteplength = np.max(steplengths)
        if maxsteplength >= self.maxstep:
            scale = self.maxstep / maxsteplength
            # FUTURE: Log this properly
            # msg = '\n** scale step by {:.3f} to be shorter than {}'.format(
            #     scale, self.maxstep
            # )
            # print(msg, flush=True)

            dr *= scale

        return dr


    def update(self, r, f, r0, f0):
        if self.H is None:
            self.H = self.H0
            return
        dr = r - r0
        df = f - f0

        if np.abs(dr).max() < 1e-7:
            # Same configuration again (maybe a restart):
            return

        dH = compute_dHessian(r=r, f=f,
                            rold=r0, fold=f0,
                            Hold=self.H,
                            mix = self.mix)

        self.H += dH



    def replay_trajectory(self, traj):
        """Initialize hessian from old trajectory."""
        if isinstance(traj, str):
            from ase.io.trajectory import Trajectory
            traj = Trajectory(traj, 'r')
        self.H = None
        atoms = traj[0]
        r0 = atoms.get_positions().ravel()
        f0 = atoms.get_forces().ravel()
        for atoms in traj:
            r = atoms.get_positions().ravel()
            f = atoms.get_forces().ravel()
            self.update(r, f, r0, f0)
            r0 = r
            f0 = f

        self.r0 = r0
        self.f0 = f0

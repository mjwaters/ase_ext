from ase import Atoms
import numpy as np




######################

def get_k_vectors_by_lambda_min(cell, lambda_min, include_zero=False):
    k_cut = 1/lambda_min
    rcell = cell.reciprocal()
    wavenumbers = np.ceil(k_cut/rcell.lengths())
    kvec_list = []
    for ki in range(1-int(wavenumbers[0]), int(wavenumbers[0])):
        for kj in range(1-int(wavenumbers[1]), int(wavenumbers[1])):
            for kk in range(1-int(wavenumbers[2]), int(wavenumbers[2])):
                wavenumvec = (ki, kj, kk)
                kvec = wavenumvec @ rcell
                klen = np.sqrt( (kvec*kvec).sum() )
                if klen/k_cut <= 1.0:
                    if include_zero:
                        kvec_list.append(kvec)
                    elif not ( ki==0 and kj==0 and kk==0):
                        kvec_list.append(kvec)
            # #print(wavenumvec, kvec, klen/k_cut)
            # if kk == 0:
            #     if klen/k_cut <= 1.0:
            #         pts_in.append(wavenumvec)
            #     else:
            #         pts_out.append(wavenumvec)
    return np.array(kvec_list)


def get_amplitude_vectors_for_k_vectors(atoms, delta_R, k_vectors):
    amplitude_vectors = np.zeros(k_vectors.shape,  dtype=complex)
    for kvec_index in range(k_vectors.shape[0]):
        for atom_index in range(len(atoms)):
            k = k_vectors[kvec_index]
            R_atom = atoms.positions[atom_index]
            amplitude_vectors[kvec_index] += delta_R[atom_index] * np.exp(-2.0j*np.pi*( np.dot(k, R_atom) ))

    return amplitude_vectors

def get_displacement_vectors_from_amplitude_vectors(
                            atoms,
                            amplitude_vectors,
                            k_vectors,
                            filter_function=None,
                            return_complex=False):
    delta_R_long = np.zeros(atoms.positions.shape, dtype=complex)
    for kvec_index in range(k_vectors.shape[0]):
        for atom_index in range(len(atoms)):
            k = k_vectors[kvec_index]
            R_atom = atoms.positions[atom_index]
            if filter_function is None:
                f = 1.0
            else:
                f = filter_function(k)
            delta_R_long[atom_index] += f * amplitude_vectors[kvec_index] * np.exp(2.0j*np.pi*( np.dot(k, R_atom) ))
    return delta_R_long/(k_vectors.size+1)*3  #/atoms.get_volume()*2 #/len(atoms) #(k_vectors.size+1) * 3#np.pi
#########################
cellx = 2
atoms = Atoms('H', scaled_positions=[[0.5,0.5,0.5]], cell = [cellx,3,7] )
#atoms = Atoms('H', scaled_positions=[[0.5,0.5,0.5]], cell = [2,2,2] )

atoms=atoms.repeat([32,1,1])

from ase.visualize import view



delta_R = np.zeros(atoms.positions.shape)
spos = atoms.get_scaled_positions()
if True:
    for i in range(len(atoms)):
        delta_R[i,1] = 1
        if spos[i,0]>0.5:
            delta_R[i,1] = -1
if True:
    for i in range(len(atoms)):
        delta_R[i,2] = -1
        if spos[i,0]>0.75:
            delta_R[i,2] = 1
        elif spos[i,0]<0.25:
            delta_R[i,2] = 1

atoms.set_velocities(delta_R)
#############


lambda_min = 16.0
kcut = 1/lambda_min
if False:
    def my_filter_function(k):
        f = 1-np.sqrt( (k**2).sum() )/kcut
        return f
if True:
    def my_filter_function(k):
        kfrac = np.sqrt( (k**2).sum() )/kcut
        f = (1-kfrac)**2
        return f

k_vectors = get_k_vectors_by_lambda_min(atoms.get_cell(), lambda_min = lambda_min, include_zero=True)
amplitude_vectors = get_amplitude_vectors_for_k_vectors(atoms, delta_R, k_vectors)
delta_R_long = get_displacement_vectors_from_amplitude_vectors(  atoms,
                            amplitude_vectors,
                            k_vectors,
                            #filter_function = None,
                            filter_function = my_filter_function,
                            )

delta_R_short = delta_R - delta_R_long

atoms_long = atoms.copy()
atoms_long.set_velocities(delta_R_long.real)
atoms_short = atoms.copy()
atoms_short.set_velocities(delta_R_short.real)

print(delta_R)
print(delta_R_long)


############
print('natoms', len(atoms))
print('k_vectors size', len(k_vectors))
print('vol', atoms.get_volume())
view([atoms, atoms_long, atoms_short])



##########
from matplotlib import pyplot as plt

tol = 0.00001
out_k = []
out_amp = []
for k_index in range(len(k_vectors)):
    k=k_vectors[k_index]
    if abs(k[1]) < tol and abs(k[2]) < tol:
        out_k.append(k)
        out_amp.append(amplitude_vectors[k_index])

out_k = np.array(out_k)
smap = np.argsort(out_k[:,0])
out_k = out_k[smap]
out_amp = np.array(out_amp)[smap]
out_kx = out_k[:,0]

############
fig, ax = plt.subplots()
ax.plot(out_kx, out_amp[:,0].real, color = 'red', linestyle='-')
ax.plot(out_kx, out_amp[:,0].imag, color = 'red', linestyle='--')

ax.plot(out_kx, out_amp[:,1].real, color = 'green', linestyle='-')
ax.plot(out_kx, out_amp[:,1].imag, color = 'green', linestyle='--')

ax.plot(out_kx, out_amp[:,2].real, color = 'blue', linestyle='-')
ax.plot(out_kx, out_amp[:,2].imag, color = 'blue', linestyle='--')

ax.set_ylabel('amp')
ax.set_xlabel('kvec')


##############
fig, ax = plt.subplots()
fout = np.zeros(len(out_k))
for kx_index in range(len(out_kx)):
    k=out_k[kx_index]
    fout[kx_index] = my_filter_function(k)


ax.plot(out_kx, fout, color = 'blue', linestyle='-')

plt.show()

import numpy as np
from ase import Atoms
from ase.lattice.cubic import FaceCenteredCubic
random_seed = 1990
rng = np.random.default_rng(seed = random_seed)

### this would be a nicer looking approach
#class OptimizerBenchmark(Atoms):
#    def __init__(self,  atoms, name, relax_cell=False, ):
#        self.name=name
#        self.relax_cell=relax_cell
#        Atoms.__init__(self )


class OptimizerBenchmark():
    def __init__(self,  atoms, name, relax_cell=False, ):
        self.atoms=atoms
        self.name=name
        self.relax_cell=relax_cell


def RandomCu():
    name = 'RandomCu'
    Natoms = 20
    L = 10.0
    spos_init = rng.random((Natoms,3))
    atoms0 =Atoms(
            'Cu'*Natoms,
            scaled_positions = spos_init,
            cell = [L,L,L],
            pbc=True)

    atoms0.set_scaled_positions(spos_init)
    atoms0.get_positions()
    relax_cell = False
    return OptimizerBenchmark(atoms0, name, relax_cell)


def Cu333():
    name = 'Cu333'
    size = 3
    atoms0 = FaceCenteredCubic(directions=[[1, 0, 0], [0, 1, 0], [0, 0, 1]],
                              symbol='Cu',
                              size=(size, size, size),
                              pbc=True)
    atoms0
    atoms0.rattle(stdev=0.18 , seed = random_seed)
    atoms0.rattle(stdev=0.18 , seed = random_seed+1)
    relax_cell = False
    return OptimizerBenchmark(atoms0, name, relax_cell)


def Cu555():
    name = 'Cu555'
    size = 5
    atoms0 = FaceCenteredCubic(directions=[[1, 0, 0], [0, 1, 0], [0, 0, 1]],
                              symbol='Cu',
                              size=(size, size, size),
                              pbc=True)
    atoms0
    atoms0.rattle(stdev=0.18 , seed = random_seed)
    atoms0.rattle(stdev=0.18 , seed = random_seed+1)
    relax_cell = False
    return OptimizerBenchmark(atoms0, name, relax_cell)

def Cu222VacCell():
    name = 'Cu222VacCell'
    size = 2
    atoms0 = FaceCenteredCubic(directions=[[1, 0, 0], [0, 1, 0], [0, 0, 1]],
                              symbol='Cu',
                              size=(size, size, size),
                              pbc=True)
    del atoms0[-1]
    atoms0.rattle(stdev=0.18 , seed = random_seed)
    atoms0.rattle(stdev=0.18 , seed = random_seed+1)
    relax_cell = True
    return OptimizerBenchmark(atoms0, name, relax_cell)




benchmark_list = [Cu333(), RandomCu(), Cu222VacCell(),  Cu555(),]


import numpy as np

def beta_grad_line_search(H, f):
    return np.dot(f,f)/(f.T @ H @ f)

def beta_grad_line_search_eigen(omega, fe):
    return np.dot(fe,fe)/(fe.T @ np.diag(omega) @ fe)

##############
def OWSe_ASE_BFGS(omega, fe):
    '''Original flavor ASE BFGS'''
    omega_weights = 1/np.fabs(omega)
    return omega_weights

def OWSe_GradientLineSearch(omega, fe):
    '''Follows gradient line for update'''
    beta = beta_grad_line_search_eigen(omega, fe)
    omega_weights = np.ones(len(omega))*beta
    return omega_weights

def OWSe_InverseSQRT(omega, fe):
    '''Original flavor ASE BFGS'''
    omega_weights = 1/np.sqrt(np.fabs(omega))
    return omega_weights

def OWSe_InverseThreeHalfPow(omega, fe):
    '''Original flavor ASE BFGS'''
    omega_weights = 1/(np.sqrt(np.fabs(omega))**3)
    return omega_weights

        # if self.omega_weight_scheme == 'ASE-BFGS-OWS':
        #     omega_weights = 1/np.fabs(omega)
        #
        # if self.omega_weight_scheme == 'GradientLS': #follows gradient line for update
        #     omega_weights = np.ones(len(omega))*alpha_grad_line_search


if False:
        #######  weight matrix
        #### this is arbitrary
        if self.omega_weight_scheme == 0:
            omega_0 = 5 # 5 is great without cell relaxation
            omega_weights = 1/(omega-omega.min() + omega_0) + omega_0

        if self.omega_weight_scheme == 6:
            omega_0 = 0.1 # 0.02 is similar to 0.1 (is wilder than 0), 2 is good without cell relaxation
            omega_weights = 1/(omega-omega.min() + omega_0) + 1/omega_0 # the baseline is neeeded!!

        if self.omega_weight_scheme == 7:
            omega_s = 70 # 70 seems okay
            omega_weights = np.exp(-(omega-omega.min())/omega_s)

        if self.omega_weight_scheme == 8:
            omega_s = 2.0/abs(alpha_grad_line_search)
            omega_weights = np.exp(-(omega-omega.min())/omega_s) #

        if self.omega_weight_scheme == 9:
            omega_s = 2.0/abs(alpha_grad_line_search)
            omega_weights = np.exp(-(omega-omega.min())/omega_s) + 1/100*omega_s # seems good ish

        if self.omega_weight_scheme == 10:
            omega_weights = 1/np.sqrt(np.fabs(omega))

        if self.omega_weight_scheme == 11:
            omega_weights = omega-omega.min()

        if self.omega_weight_scheme == 12:
            omega_weights = np.fabs(omega)

        if self.omega_weight_scheme == 13:
            omega_weights = 1/(np.fabs(omega)**2)

        if self.omega_weight_scheme == 14:
            omega_weights = 1/(omega-omega.min() + 1e-3)**2

        if self.omega_weight_scheme == 1:
            omega_0 = 0.5/abs(alpha_grad_line_search) # try  0.1, 2.0 Does ABS affect this?
            omega_weights = 1/(omega-omega.min() + omega_0) + omega_0

        if self.omega_weight_scheme == 2:
            omega_0 = 0.05/abs(alpha_grad_line_search) # try  0.1, 2.0 Does ABS affect this?
            omega_weights = 1/(omega-omega.min() + omega_0)

        if self.omega_weight_scheme == 3:
            omega_0 = 0.01/alpha_grad_line_search # try 0.01 was  best without the cell, 0.05, 0.002 isn't much worse, ABS ruins this???
            omega_weights = 1/np.sqrt( ((omega-omega.min() + omega_0)/omega_0)) +  np.sqrt(omega_0)

        if self.omega_weight_scheme == 4:
            omega_baseline = 0.1  # 0.1 works, what's best?

            omega_scaled = (omega-omega.min())/(omega.max()-omega.min()+1e-6)

            omega_weights = (1-omega_baseline)*(1-omega_scaled) + omega_baseline

        if self.omega_weight_scheme == 5:
            omega_baseline = 0.05 #
            omega_scaled = (omega-omega.min())/(omega.max()-omega.min()+1e-6)
            omega_sphere = 1 - np.sqrt(1-(omega_scaled-1.0)**2)
            #print(omega_sphere)
            omega_weights = (1-omega_baseline)*(omega_sphere) + omega_baseline
